var fs = require("fs");

function findS(n, phi){
	var s=1;
	while(s<(z-1)){
		if((n*s)%phi==1){
			return s
		}
		s++
	}
	return 0
}

function expomod(a, n, z){
	var i = n
	var r = 1
	var x = a%z
	var iter =0
	while(i>0){
		if(i%2==1){
			r=(r*x)%z
		}
		x=(x*x)%z
		i=Math.floor(i/2)
		iter++
	}
	return r
}

function encrypt(message){
	if(typeof message !='string'){
		console.log("message is not string")
		return
	}
	var string =""
	for(var i=0;i<message.length;i++){
		var uni =message.charCodeAt(i)
		string+=expomod(uni, n, z)
		string+="-"
	}
	//fs.writeFile('crypted.txt', string, 'utf8');
	//console.log("encrypted")
	return string
}


function decrypt(coded){
	if(typeof coded !='string'){
		console.log("message is not string")
		return
	}
	string=""
	var int=0
	var balise=0
	for(var i=0;i<coded.length;i++){
		if(coded.charAt(i)=='-'){
			int = +coded.substring(balise,i)
			var decoded = expomod(int, s, z)
			string+= String.fromCharCode(decoded)
			balise=i+1
		}
			
	}
	//fs.writeFile('decrypted.txt', string, 'utf8');
	//console.log("decrypted");
	return string
}

var p=3719
var q=3907
var z=p*q
var phi=(p-1)*(q-1)//public
var n=1951//bob
var s = findS(n, phi)//alice


module.exports={
	encrypt:encrypt,
	decrypt:decrypt
}
