$(document).ready(function(){
	$.ajax({
		type: 'POST',
		url: '/getHistory',
		data: {input:$('#textInput').val()},
		success: function(result) {
			displayChat(result)
		},
	});

	$('#textInput').keypress(function (e) {
		var key = e.which;
		if(key == 13 && $("#textInput").val()!=''){

			send();
		}
	});
})



function send(){
	$.ajax({
		type: 'POST',
		url: '/sendChat',
		data: {name:$('#name').val(), text:$('#textInput').val()},
		success: function(result) {
			displayChat(result)
			$("#textInput").val("")
		},
	});
}

function displayChat(obj){
	var chat=""
	for(var key in obj){
		chat+="<span class=date>"+key+"</span><br>"
		if(validURL(obj[key])){
			chat+=parseUrl(obj[key]);
       }else{
       	chat+=obj[key];
       }
       if(obj[key].includes(".jpg")||obj[key].includes(".jpeg")||obj[key].includes(".png")||obj[key].includes(".gif")){
       	chat+=parseImg(obj[key])
       }
      chat+="<hr align=left>"


   }
   $('div.mainText').html(chat);

   var d = $('div.mainText');
   d.scrollTop(d.prop("scrollHeight"));
}

function parseUrl(text){
	var index = text.indexOf("http");
	var debut = text.substring(0,index);
	var result = debut+"<br><a href='"
	var link=''
	for(var i=index;i<text.length;i++){
		link+=text.charAt(i);
	}
	result+=link+"\' target=\'_blank\'>"+link+"</a><br><br>"
	return result
}

function parseImg(text){
	var index = text.indexOf("http");
	var result="<img src='"
	var link=''
	for(var i=index;i<text.length;i++){
		link+=text.charAt(i);
	}
	result+=link+"'>"
	
	return result
}
//<iframe src="https://giphy.com/embed/dn06z88hDE2di5RBfg" width="480" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/film4-thinking-jeff-goldblum-dn06z88hDE2di5RBfg">via GIPHY</a></p>
function parseGif(text){
	var index = text.indexOf("http");
	var result="<iframe src='"
	var link=''
	for(var i=index;i<text.length;i++){
		link+=text.charAt(i);
	}
	//result+=link+"width="480" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/film4-thinking-jeff-goldblum-dn06z88hDE2di5RBfg">via GIPHY</a></p>"
	console.log(result)
	return result
}

function validURL(str) {
	var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
	if(!regex .test(str)) {
		console.log(false)
		return false;
	} else {
		console.log(true)
		return true;
	}
}