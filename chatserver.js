var fs = require("fs");
var path = require("path");
var history = require('./public/history.json')

var express = require('express')
var app = express()
const bodyParser = require('body-parser');
const crypter=require('./crypt.js');

app.set('trust proxy', true)
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json('application/json'));

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname+'/index.html'))
});

app.post('/getHistory',function (req, res) {
    res.send(decryptHistory());
});


app.post('/sendChat',function (req, res) {
    console.log(req.body);
    addHistory(req.body.name,req.body.text);
    res.send(decryptHistory());
});


function decryptHistory(){
    var real = {}

    for(var key in history){
        real[crypter.decrypt(key)]=crypter.decrypt(history[key])
    }
    return real;
}


function addHistory(name, text){
    if(name==""){
        name="Anonymous"
    }
    var nom = name +" said: "
    var now = getDate();
    now+=" || "+nom;
    history[crypter.encrypt(now)] = crypter.encrypt(text);
    saveHistory();
}

function saveHistory(){
    var json = JSON.stringify(history);
    fs.writeFile('./public/history.json', json, 'utf8', function(err){if(err)console.log(err);});
}

function getDate(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1;
    var yyyy = today.getFullYear();
    var hours=today.getHours()-4;
    var min=today.getMinutes();
    var sec = today.getSeconds();

    return  today = (mm<10 ? '0'+mm: mm) + '/' + (dd<10 ? '0'+dd: dd)+ '/' + yyyy+" "+
        (hours<10 ? '0'+hours: hours)+":"+(min<10 ? '0'+min:min)+":"+(sec<10 ? '0'+sec: sec);
}



app.listen(3000, () => console.log('Now listening to 3000'));